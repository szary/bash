#!/bin/bash

# okienka w bashu
# ::xbash::
# ver. 20181214

# TODO: 
# Array like xbash_id , xbash_id=([pos_x]=%, [pos_y]=%, [title]=$, [..and etc.]=? ) 

# DEBUG
# set -x

####################### module::xbash[START] ###############################

### xbash:tput:cols_and_rows 
## get term config

function tput_rows_cols() {
   tput_rows=`tput lines`
   tput_cols=`tput cols`
}


#### xbash::bg

function xbash_bg() {

   ### xbash::size:variables: ${w_height} ${w_width} 
   ## rows cols
   local bg_position_rows="${_position_rows}"
   local bg_position_cols="${_position_cols}"  
   
   for bg_line in `seq 1 "${w_height}"`; do
      
      # xbash::field_title
      if [[ "${bg_line}" -eq 1 ]] ; then
         
         tput cup "${bg_position_rows}" "${bg_position_cols}" 
         tput setab "${_title_setab}"
         printf "%${w_width}s" # rysujemy tlo
         tput sgr0 # reset

      # xbash::field_text
      else
         tput setab "${w_setab}" # set background-color
         tput cup "${bg_position_rows}" "${bg_position_cols}" 
         printf "%${w_width}s" # 
         tput sgr0
      fi
      
      (( bg_position_rows++ )) # line by line
   done
   
}

#### xbash:main


#### xbash::txt_fill

function xbash_txt_fill() {

   
   local txt_position_rows=$(( "${_position_rows}" + 1 )) # 1st line for title, text start from 2nd line
   local txt_position_cols="${_position_cols}"  

   ## //title\\ ##

   
   # title::text
   tput cup "${_position_rows}" "${txt_position_cols}"
   tput setab "${_title_setab}"
   printf "%${_title_text_padding}s ${_title_text}"
   tput sgr0

   # title::icon
   tput cup "${_position_rows}" "${_position_cols}"
   tput setab "${_title_icon_setab}"
   tput setaf "${_title_icon_setaf}"
   echo "${_title_icon}" # icon / shortcut
   tput sgr0


   ## \\title// ## 

   OLD_IFS=$IFS 
   IFS=$'\n'


   # text
   for txt_line in `echo "${_text}"` ; do 

      tput setab "${w_setab}"
      tput cup "${txt_position_rows}" "${txt_position_cols}"
      printf "%${_text_padding}s ${txt_line}"
      (( txt_position_rows++ )) # subshell increment

   done 

   IFS=${OLD_IFS} # restore defaults IFS

}


function xbash() {
   
   tput sc # save cursor pos
   tput setaf 1
   xbash_bg 
   xbash_txt_fill 

   tput rc # restore cursor pos 
}

#### xbash::init
function xbash_init() {

      ### INFO ------------------------------------------ 
      ## $1 - pos.Y
      ## $2 - pos.X
      ## $3 - width[%]
      ## $4 - title
      ## $5 - text
      ### ------------------------------------------

   tput_rows_cols # pobieramy aktualne dane : wys i szer terminala  

   ###/CONFIG/###

   # xbash:config:txt_fill zakladamy, iz zmienne sa prawidlowe
   _width=$(( ( "${tput_cols}" * $3 ) / 100 )) # procent szerokosci wyrazony liczbowo i zaokraglony do liczby calkowitej
   _height=$( echo "$5" | fold -w"${_width}" -s | wc -l ) # resizable - adaptating to xbash size
   _text=$( echo "$5" | fold -w"${_width}" -s )
   _text_padding=1 # left padding
   _title_text=$( echo "$4" | fold -w"${_width}" -s | head -n1 ) # title: obcinanie tekstu tak aby zmiescil sie w dopuszczalnym rozmiarze ( 1 linia , szerokosc okienka ) 
   _title_text_length=$( echo ${#_title_text} )
   _title_text_padding=$(( ( ( "${_width}" - "${_title_text_length}" - 1) / 2 ) + 1 )) #    

   # xbash:config:bg # zwiekszamy wielkosc wyrysowanego okienka, a dokladniej tla ( + 2 ) 
   w_width=$(( "${_width}" + 2 ))
   w_height=$(( "${_height}" + 2 ))
   
   # xbash:config:colors # 
   _title_setab="88" # xbash::title:bg:color
   _title_setaf="167" # xbash::title:text:color
   w_setab="28" # xbash::txt:background:color 
   w_setaf="257" # xbash::txt:font:color 
   
   # xbash:config:pos_Y_X
   _position_rows=${1:-10}
   _position_cols=${2:-10}

   # xbash:config:title:icon
   _title_icon=" X "
   _title_icon_setaf="1"
   _title_icon_setab="3"

   ###\CONFIG\###

   # xbash::create_window
   xbash 

}


####################### MODULE:xbash [END] ##########################

# example:
xbash_init 20 50 21 "Lorem Ipsum" "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.  "  
