#!/usr/bin/env bash

IFS=$'\n'

template='
## tutorial

## find /sys/devices -type f -name "temp*_input"
## dopisujemy hwmon przed /sys/devices...


#### archlinux forum
#tp_thermal /proc/acpi/ibm/thermal (0, 0, 0, 0, 0, 0, 0, 0)

#(0,    0,    45)
#(1,    43,    50)
#(2,    40,    51)
#(3,    42,    53)
#(4,    46,    55)
#(5,    49,    56)
#(7,    43,    32767)
####


# for i in $(find /sys/devices -type f -name "temp*_input");do echo "hwmon ${i}" ; done | grep -E 'hwmon5.temp8' -v


tp_fan /proc/acpi/ibm/fan
tp_thermal /proc/acpi/ibm/thermal (0, 0, 0, 0, 0, 0, 0, 0)



(0,     0,      42)
(1,     40,     47)
(1,     45,     52)
(2,     50,     57)
(3,     55,     62)
(5,     60,     77)
(7,     73,     93)
(127,   85,     32767)
#old thresholds

#(0,	0,	45)
#(1,	45,	55)
#(2,	55,	65)
#(3,	65,	75)
#(4,	75,	80)
#(6,	80,	85)
#(7,	85,	32767)
'

hwmons=$(for i in $(find /sys/devices -type f -name "temp*_input");do echo "hwmon ${i}" ; done | grep -E 'hwmon5.temp8' -v)


thinkfanconf=$(echo -e "${template}\n${hwmons}")

echo -e "${thinkfanconf}" > /etc/thinkfan.conf
systemctl restart thinkfan
systemctl status  thinkfan
