#!/usr/bin/env bash
# szaraq
# set -x #;;debug
#
# openssl crl2pkcs7 -nocrl -certfile filenane.pem| openssl pkcs7 -print_certs -text -noout 
# openssl x509 -in filename.crt -text -noout


function usage(){

   echo -e "Usage: ${0} hostname[remote|localhost]
       test1 test2"

}

function find_cert_files_and_check() {

   paths=()
   paths+=('/virtual/*/etc/')
   paths+=('/etc/')
   paths+=('/usr/local/share/ca-certificates/')

   regex='2021'
#   paths+=('/')
   
   echo -e "!!!!!!!!!!!!!!!!!!!"
   echo -e "Paths: ${paths[*]}"
   echo -e "Regex: ${regex}"
   echo -e "!!!!!!!!!!!!!!!!!!!"


   for path in ${paths[*]}; do 
      
      if [[ -d ${path} ]] ; then

         for file in $(find ${path}  -type f -name "*.pem" -o -name "*.crt"); 
         do 
            echo ${file}
            if [[ ${file##*.} == "pem" ]] ; then

               matches="$(openssl crl2pkcs7 -nocrl -certfile ${file}| openssl pkcs7 -print_certs -text -noout  | egrep ${regex})"
               [[ -n ${matches} ]] && echo -e "\t${host}>>${file}" && echo ${matches}

            elif [[ ${file##*.} == "crt" ]] ; then

               matches="$(openssl x509 -in ${file} -text -noout  | egrep ${regex})"
               [[ -n ${matches} ]] && echo -e "\t${host}>>${file}" && echo ${matches}

            else
               echo "Cos poszlo nie tak"

            fi 
         done
      fi
   done

}


function main() {


   [[ ! -n ${1} ]] && echo -e "Brak hosta" && usage &&  exit 200
#   host="root@${1##*@}"
   host="${1}"

   echo ${host}
   if [[ ${host} == 'localhost' ]] ; then
      echo '--------------------------------'
      echo "$(hostname) :";
      echo '--------------------------------'
      find_cert_files_and_check
      echo '--------------------------------'

   else

      ssh -T  ${host} <<DISC
      host=${host}
   
      $(typeset -f find_cert_files_and_check)
      find_cert_files_and_check
DISC
   fi



}

main "$@"



