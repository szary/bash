#!/usr/bin/env bash

#eans=()
#count=0

function fetch_ean() {

   eans=()

   for ean in $(find -name '*.jpg' -type f | cut -d'_' -f1 | sort  | uniq); do
      eans+=("${ean##./}")
   done

  echo -n "Ilość unikatowych eanow: ${!eans[*]}"
  echo ""
  echo -n "${eans[*]}"
}


function ean_search_files_by_prefix() {
   
   prefix=${ean_prefix}
   for ean_prefix in ${eans[*]}; do 
      echo -n "-----${ean_prefix}--------\n"
      count=1
      for filename in $(find -name "${ean_prefix}_*" -type f); do
         echo "${ean_prefix}_${count}.jpg"
         ((count++))
      done
         
   done
} 


fetch_ean
ean_search_files_by_prefix
