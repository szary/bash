!/bin/bash
macs=(); for maczek in $( ip link sh | grep 'link/ether' | awk '{print $2}' ); do macs+=(${maczek}) ; done
echo "
network:
  version: 2
  renderer: networkd
  ethernets:
    lo:
      addresses:
        - 10.255.0.1/32
    eth0:
      dhcp4: yes
      match:
         macaddress: ${macs[0]}
    eth1:
      dhcp4: yes
      mtu: 9000
      match:
         macaddress: ${macs[1]}" > /etc/netplan/01-netcfg.yaml

