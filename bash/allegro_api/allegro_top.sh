#!/bin/bash


# set -x # debug
# base64 ;; echo -n - do not output the trailing newline

# categories list
# curl -X GET -H "Authorization: Bearer ${ccflow_access_token}" -H "Accept: application/vnd.allegro.public.v1+json" 'https://api.allegro.pl/sale/categories' | jq

# subcategories
# curl -X GET -H "Authorization: Bearer ${ccflow_access_token}" -H "Accept: application/vnd.allegro.public.v1+json" 'https://api.allegro.pl/sale/categories?parent_id=3919' | jq


##// CONFIG 

ccflow_data_file="ccflow_data_nogit"

source .allegro_data.nogit # untrusted fiel // client_id client_secret base64_credentials 
#client_id="" #
#client_secret="" 
#base64_credentials="" #  client_id:client_secret (  base64 echo -n  )
_url='https://allegro.pl'
_url_oauth="${_url}/auth/oauth"
_url_oauth_device="${_url_oauth}/device"
_url_oauth_token="${_url_oauth}/token?"
_header_contype_urlencoded='Content-Type: application/x-www-form-urlencoded'
_header_auth_basic="Authorization: Basic" 
_header_auth_bearer="Authorization: Bearer"
_header_public_v1="Accept: application/vnd.allegro.public.v1+json"
_header_contype_v1="content-type: application/vnd.allegro.public.v1+json"

uri_api='https://api.allegro.pl'
uri_sale_cat='sale/categories?parent.id='
uri_offers_list='offers/listing?'

declare -A filter=( [limit]='10' [sort]='-popularity' )

##\\-CONFIG


# list offers, doc: https://developer.allegro.pl/news/2018-07-03-listing_ofert/


function filter_manager::info() {

   echo "
      
   example: 

      filter[limit]=10
         
   filters:

      category.id
      phrase
      limit
      sort
      include
      offset
   "
      

}


function filter_manager() {

  fm_opt=$1 
  filter_uri='' # reset filter_URI

  # _show - current settings

  if [[ "${fm_opt}" == "show" ]] || [[ "${fm_opt}" == '' ]]; then

     # generowanie uri
     filter_manager gen

     for i in ${!filter[@]} ;do
         echo "${i} = ${filter[${i}]}"
     done
     
     echo "Filter uri: ${filter_uri}"
  fi

  # _generate - create uri
  # filter[limit]=20 && filter_manager gen

  if [[ "${fm_opt}" == "gen" ]]; then

    for key in ${!filter[@]} ; do
       if [[ -n ${filter[${key}]} ]]; then
          filter_uri+="&${key}=${filter[${key}]}"
       fi
    done

  fi

}

function list_offers() {

   category_id=$1

   filter_manager gen
   echo "Filters have been updated"
   echo "TOP offers by category ID"
   sleep 1s
   curl -X GET -H "${_header_contype_v1}" -H "${_header_auth_bearer} ${ccflow_access_token}" -H "${_header_public_v1}" "${uri_api}/${uri_offers_list}category.id=${category_id}${filter_uri}" | jq '.items[]' > lastresult.log_nogit
   cat lastresult.log_nogit | head
   cat lastresult.log_nogit | jq ' . | map(.name + "|" + .id )'
#   cat lastresult.log | jq -r '. | to_entries'

}

# categories
function get_categories() {

   parent_id=$1
   #curl -X GET -H "${_header_auth_bearer} ${ccflow_access_token}" -H "${_header_public_v1}" "${uri_api}/${uri_sale_cat}${parent_id}" | jq '.categories[] | .name , .id , .leaf '
   #curl -X GET -H "${_header_auth_bearer} ${ccflow_access_token}" -H "${_header_public_v1}" "${uri_api}/${uri_sale_cat}${parent_id}" | jq '.categories[] | .name + " | " +.id + map(select(.leaf))' 
   
   curl -X GET -H "${_header_auth_bearer} ${ccflow_access_token}" -H "${_header_public_v1}" "${uri_api}/${uri_sale_cat}${parent_id}" | jq '.categories[] '
   curl -X GET -H "${_header_auth_bearer} ${ccflow_access_token}" -H "${_header_public_v1}" "${uri_api}/${uri_sale_cat}${parent_id}" | jq '.categories[] | select(.leaf==false) | .name + "|" +.id' 
   echo -e "Leafs: "
   curl -X GET -H "${_header_auth_bearer} ${ccflow_access_token}" -H "${_header_public_v1}" "${uri_api}/${uri_sale_cat}${parent_id}" | jq '.categories[] | select(.leaf==true)' | tee /dev/tty > categories.last_nogit
}


########################
# TOKEN: Client_Credentials flow , docs : https://developer.allegro.pl/auth/#app

function ccflow_get_token() {

   ccflow_token_response=$(curl -X POST "${_url_oauth_token}grant_type=client_credentials" -H "${_header_auth_basic} ${base64_credentials}" )
   echo "${ccflow_token_response}" > "${ccflow_data_file}" 
   echo "Response data save to file >  ${ccflow_data_file} "
   token_expires_date=`date -d "+12 hours"`
   echo "Token expires: ${token_expires_date}" #>> "${ccflow_data_file}"

}

function ccflow_fetch_token() {
   
   echo "Getting token... " 
   ccflow_access_token=$(cat "${ccflow_data_file}" | jq '.access_token' | sed 's/\"//g')

   if [[ -n "${ccflow_access_token}" ]]; then
      echo "Success!"
      echo "${ccflow_access_token}"
      cat "${ccflow_data_file}" | tail -n1
   else
      echo "Uppps :( Check file with access token"
   fi

}

