#!/usr/bin/env python

import hashlib
import argparse




if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--id", help="Podaj imageid, zwraca nazwę pliku obrazu", required=True)
    args = parser.parse_args()

    imageid = args.id
    print(hashlib.sha1(imageid).hexdigest())

