#!/usr/bin/env python

# N.Bielka
# Skrypt nie dziala ? doinstaluj modul openpyxl
# /usr/bin/pip install openpyxl --user

from jira import JIRA
from openpyxl import Workbook
from openpyxl import load_workbook
import requests
import datetime

ad_auth = ('login', 'password')
reports_filepath = '/path/to/reports'
reports_filelist = []
jira_server = {'server': 'https://...'}
jira = JIRA(jira_server, basic_auth=(ad_auth))
jira_main_issue = 'main_issue_id'
reported_names = []
attachments_path = '/path/to/save/attachment'


def fetch_activenames(filelist):
    """ parse xls  """
    for reportxls in reports_filelist:
        print('file: {}'.format(reportxls))
        rwb = load_workbook(reports_filepath + reportxls)
        rws = rwb.active
        rmaxrow = rws.max_row

        for y in range(2, rmaxrow+1):
            """ skip first row and set cursor to column 2 """

            cell_obj = rws.cell(row=y, column=2)
            reported = cell_obj.value
            reported_names.append(reported)

        print("Count: : {}".format(len(reported_names)))


def save_reported(activenames):
    """ save to file """

    with open(reports_filepath + 'reported.txt', 'w') as file:
        file.writelines('\n'.join(activenames))



if __name__ == '__main__':

    print('Finding subtasks  {}'.format(jira_main_issue))
    podzadania = jira.issue(jira_main_issue).fields.subtasks
    print('Found: {}'.format(podzadania))

    print('Download attachments')

    for issue in podzadania:

        print(issue.key)
        for zalacznik in jira.issue(issue.key).fields.attachment:
            print(zalacznik.raw['content'])
            attach_url = str(zalacznik.raw['content'])
            attach_name = str(zalacznik.filename)
            res = requests.get(attach_url, auth=(ad_auth))
            open(attach_name, 'wb').write(res.content)
            reports_filelist.append(attach_name)

    fetch_activenames(reports_filelist)
    save_reported(reported_names)

