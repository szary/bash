#!/usr/bin/env python3

import click



@click.command()
@click.argument('arg1', default='pusty')
def hello(arg1):
    click.secho(f' {arg1} argument', fg='yellow', bold=True)

if __name__ == '__main__':
    hello()
