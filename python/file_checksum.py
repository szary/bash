2#!/usr/bin/env python3

import hashlib
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--file', required=True)
    parser.add_argument('--algorithm', choices=['md5','256','512'], default='512')

    args = parser.parse_args()
    filename = args.file
    algorithm = args.algorithm


    def checksum(filename, algorithm):
        algorithms = {"md5": hashlib.md5,
                      "256": hashlib.sha256,
                      "512": hashlib.sha512,
                      }
        with open(filename,"rb") as f:
            bytes = f.read() # read file as bytes
            readable_hash = algorithms[algorithm](bytes).hexdigest();
        print(readable_hash)

    checksum(filename, algorithm)


"""
    def checksum(filename, algorithm):
        algorithms = {"md5": hashlib.md5,
                      "256": hashlib.sha256,
                      "512": hashlib.sha512,
                      }
        with open(filename, "rb") as f:
            m = algorithms[algorithm]()
            for chunk in iter(lambda: f.read(65536), ''):
                m.update(chunk)
        print("%s: %s" % (algorithm, m.hexdigest()))
"""

