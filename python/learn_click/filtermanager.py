#!/usr/bin/env python2

import click

filters = {1:'jeden', 2:'dwa'}

@click.group("cli")
@click.pass_context
@click.argument("filters")
def cli(ctx, filters):
    test = {}
    ctx.obj = _dict

@cli.command("get_keys")
def get_keys(_dict):
    keys = list(_dict.keys())
    click.secho("Filtry: ")
    click.echo(click.style(keys, fg=blue))

def main():
    cli(prog_name="cli")


if __name__ == '__main__':
    main()
